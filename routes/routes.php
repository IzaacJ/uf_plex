<?php
// Sprinkle General routes
$app->get('/libraries', 'UserFrosting\Sprinkle\Plex\Controller\PlexController:showLibraries')
    ->add('authGuard');
$app->get('/library/{id}', 'UserFrosting\Sprinkle\Plex\Controller\PlexController:showLibrary')
    ->add('authGuard');
$app->get('/movie/{id}', 'UserFrosting\Sprinkle\Plex\Controller\PlexController:showMovie')
    ->add('authGuard');

// Sprinkle API routes
$app->group('/api/plex', function() {
    // Validate a Plex Token
    $this->get('/tokenvalid', 'UserFrosting\Sprinkle\Plex\Controller\PlexApiController:getTokenValid');

    // Library Information
    $this->get('/libraries', 'UserFrosting\Sprinkle\Plex\Controller\PlexApiController:getLibraries')
        ->add('authGuard');
    $this->get('/library/{id}/info', 'UserFrosting\Sprinkle\Plex\Controller\PlexApiController:getLibraryInfo')
        ->add('authGuard');
    $this->get('/library/{id}', 'UserFrosting\Sprinkle\Plex\Controller\PlexApiController:getLibrary')
        ->add('authGuard');

    $this->get('/movie/{id}', 'UserFrosting\Sprinkle\Plex\Controller\PlexApiController:getMovie')
        ->add('authGuard');
});

// Sprinkle Modal routes
$app->group('/modals/plex', function() {
    $this->get('/howto', 'UserFrosting\Sprinkle\Plex\Controller\PlexModalController:getHowTo');
});