<?php
namespace UserFrosting\Sprinkle\Plex\Database\Migrations\v100;

use UserFrosting\System\Bakery\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

use UserFrosting\Sprinkle\Account\Database\Models\Permission;

class PlexPermissions extends Migration {
    public $dependencies = [
        '\UserFrosting\Sprinkle\Account\Database\Migrations\v400\RolesTable',
        '\UserFrosting\Sprinkle\Account\Database\Migrations\v400\PermissionsTable'
    ];

    public function up() {
        foreach($this->plexPermissions() as $permissionInfo) {
            $permission = new Permission($permissionInfo);
            $permission->save();
        }
    }

    public function down() {
        foreach($this->plexPermissions() as $permissionInfo) {
            $permission = Permission::where($permissionInfo)->first();
            $permission->delete();
        }
    }

    protected function plexPermissions() {
        return [
            [
                'slug' => 'see_plex_token',
                'name' => 'See Plex Tokens',
                'conditions' => 'always()',
                'description' => 'Enables the user to see anyone elses Plex Token.'
            ],
            [
                'slug' => 'edit_plex_token',
                'name' => 'Edit Plex Tokens',
                'conditions' => 'always()',
                'description' => 'Enables the user to edit anyone elses Plex Token. (DANGEROUS!)'
            ]
        ];
    }
}