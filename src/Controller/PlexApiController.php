<?php

namespace UserFrosting\Sprinkle\Plex\Controller;

use Interop\Container\ContainerInterface;
use Slim\Http\Response;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use GuzzleHttp;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\UserProfile\Database\Models\ProfileFields;

class PlexApiController extends PlexController {
    protected $ci;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->ci = $ci;
    }

    // Evaluate exceptions and return default error messages!
    private function evaluateRequest($request, $response, $exception) {
        switch($exception->getCode()) {
            case 401:
                $message = 'Invalid token.';
                break;
            case 404:
                $message = 'The Plex server seems to be down. Please try again later!';
                break;
            case 500:
                $message = 'The server had a hiccup. Contact the admin!';
                break;
            default:
                $message  = 'Something went terribly wrong. Please contact admin with below information!<br>';
                $message .= 'Exception: ' . $exception->getCode() . ' - ' . $exception->getMessage() . '<br>';
                $message .= 'File (line): ' . $exception->getFile() . '(' . $exception->getLine() . ')';
                break;
        }
        return $response->write(json_encode(array('message' => $message)))->withStatus($exception->getCode());
    }

    private function sprunjify($data, $size, $page, $sorttype, $sortdirection, $listable = null, $filter = null) {
        $count = count($data);

        // filtering
        if ($filter !== null) {
            foreach ($data as $key => $movie) {
                $match = false;

                if (isset($filter['_all'])) {
                    foreach($movie as $field => $value) {
                        if (stripos($value, $filter['_all']) !== false) {
                            $match = true;
                        }
                    }
                }else {
                    $match = 0;
                    foreach ($filter as $field => $value) {
                        //return $field;
                        if (stripos($movie[$field], $value) !== false) {
                            $match++;
                        }
                    }
                    if($match >= count($filter)) {
                        $match = true;
                    }else{
                        $match = false;
                    }
                }

                if (!$match) {
                    unset($data[$key]);
                }
            }
        }

        $count_filtered = count($data);

        // sorting
        usort($data, function ($a, $b) use ( $sorttype, $sortdirection ) {
            switch($sortdirection) {
                case "desc":
                    return strcmp(strtolower($b[$sorttype]), strtolower($a[$sorttype]));
                    break;
                default:
                    return strcmp(strtolower($a[$sorttype]), strtolower($b[$sorttype]));
                    break;
            }
        });

        // pagination
        if ($count > $size) {
            $data = array_slice($data, ($page * $size), $size);
        }

        $sprunj = array(
            "count" => $count,
            "count_filtered" => $count_filtered,
            "rows" => $data
        );

        if ($listable !== null) {
            $sprunj['listable'] = $listable;
        }

        return $sprunj;
    }

    private function ageRating($contentRating) {
        switch($contentRating) {
            case "Approved":
            case "Passed":
                return "Approved/Passed";
            case "KT":
            case "Livre":
            case "TE":
            case "A":
            case "S":
            case "o.Al.":
            case "I":
            case "L":
            case "T":
            case "U":
            case "AL":
            case "A.G":
            case "All":
            case "E":
            case "Btl":
            case "G":
                return "Everyone";
            case "PG":
            case "6":
            case "7":
            case "9":
            case "11":
            case "12":
            case "-12":
            case "12A":
            case "12PG":
            case "13":
            case "13+":
            case "14":
            case "14A":
            case "15":
            case "15A":
            case "15PG":
            case "K-8":
            case "K-8/K-5":
            case "K-10":
            case "K-10/K-7":
            case "K-12":
            case "K-12/K-9":
            case "K-13":
            case "K-14":
            case "IIA":
            case "LH":
            case "VM14":
            case "GY":
            case "RP13":
            case "R13":
            case "PG-13":
            case "R-13":
            case "M/4":
            case "M/6":
            case "M/12":
            case "I.C.-14":
                return "Parental Guidance (4-15)";
            case "16":
            case "16+":
            case "K-16":
            case "IIB":
            case "RP16":
            case "R16":
            case "M/16":
            case "NC16":
                return "Young Adult (16-18)";
            case "R":
            case "18":
            case "18+":
            case "18A":
            case "K-18":
            case "III":
            case "VM18":
            case "18SG":
            case "18SX":
            case "18PA":
            case "18PL":
            case "R-18":
            case "M/18":
            case "I.M.-18":
            case "M18":
            case "R(A)":
                return "Adults (18+)";
            case "R21":
                return "21 and above";
            case "X":
            case "A":
            case "XXX":
            case "R18":
                return "Banned/Pornography";
            case "":
            case "Unrated":
            case "Not Rated":
            default:
                return "Not Rated";
        }
    }

    // Validate token during registration!
    public function getTokenValid($request, $response, $args) {
        try {
            $params = $request->getQueryParams();
            $token = trim($params['plex_token']);

            $client = $this->getClient($token);

            // Check if token can access libraries
            $res = $client->get('library/sections');

            // Check if token is already used
            $user = ProfileFields::where('slug', 'plex-token')->where('value', $token)->value('parent_id');
            if($user !== null) {
                $message = 'Token already used! Don\'t impersonate someone!';
            }else {
                $message = true;
            }
        }catch(GuzzleHttp\Exception\ClientException $exception) {
            switch($exception->getCode()) {
                case 401:
                    $message = 'Invalid token.';
                    break;
                case 404:
                    $message = 'The Plex server seems to be down. Please try again later!';
                    break;
                case 500:
                    $message = 'The server had a hiccup. Contact the admin!';
                    break;
                default:
                    $message = 'Something went terribly wrong. Please contact admin!';
                    break;
            }
        }
        if($message === true)
            return $response->write('true')->withStatus(200);

        return $response->write($message)->withStatus(200);
    }

    // Get all libraries information
    public function getLibraries($request, $response, $args) {
        // prep query for sprunje compatability
        $params = $request->getQueryParams();
        $sp_size = (isset($params['size']) ? $params['size'] : 10);
        $sp_page = (isset($params['page']) ? $params['page'] : 0);
        $sp_sorttype = "name";
        $sp_direction = "asc";
        $sp_filter = null;
        if (isset($params['sorts'])) {
            foreach ($params['sorts'] as $type => $direction) {
                $sp_sorttype = $type;
                $sp_direction = $direction;
                break;
            }
        }
        if (isset($params['filters'])) {
            $sp_filter = $params['filters'];
        }
        $libraries = array();
        $listable = array(
            'type' => array()
        );
        $types = array();
        try {
            $res = $this->getClient()->get('library/sections');
            $directories = new \SimpleXMLElement($res->getBody()->getContents());

            foreach($directories as $directory) {
                $name = ucfirst((string)$directory['title']);
                $type = ucfirst((string)$directory['type']);
                $id = (int)$directory->Location['id'];
                $libraries[] =  array('id' => $id, 'type' => $type, 'name' => $name);
                if (array_search($type, $types) === false) {
                    $types[] = $type;
                }
            }
            foreach ($types as $cr) {
                $listable['type'][] = array("value" => $cr, "text" => $cr);
            }
        }catch(GuzzleHttp\Exception\ClientException $exception) {
            return $this->evaluateRequest($res, $response, $exception);
        }
        return $response->write(json_encode($this->sprunjify($libraries, $sp_size, $sp_page, $sp_sorttype, $sp_direction, $listable, $sp_filter), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT))->withStatus(200);
    }

    // Get library information
    public function getLibraryInfo($request, $response, $args) {
        // get which library by id
        $lib_id = (isset($args['id']) ? $args['id'] : null);
        $library = array();
        try {
            $res = $this->getClient()->get('library/sections');
            $directories = new \SimpleXMLElement($res->getBody()->getContents());

            foreach($directories as $directory) {
                if ((int)$directory->Location['id'] === (int)$lib_id) {
                    $library = array(
                        "id" => (int)$directory->Location['id'],
                        "name" => ucfirst((string)$directory['title']),
                        "type" => ucfirst((string)$directory['type'])
                    );
                    break;
                }
            }
            if ($library === array()) {
                $library = array("message" => "Unknown library ID: " . $lib_id);
            }
        }catch(GuzzleHttp\Exception\ClientException $exception) {
            return $this->evaluateRequest($res, $response, $exception);
        }
        return $response->write(json_encode($library, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT))->withStatus(200);
    }

    // Get library content
    public function getLibrary($request, $response, $args) {
        // prep query for sprunje compatability
        $params = $request->getQueryParams();
        $sp_size = (isset($params['size']) ? $params['size'] : 10);
        $sp_page = (isset($params['page']) ? $params['page'] : 0);
        $sp_sorttype = "name";
        $sp_direction = "asc";
        $sp_filter = null;
        if (isset($params['sorts'])) {
            foreach ($params['sorts'] as $type => $direction) {
                $sp_sorttype = $type;
                $sp_direction = $direction;
                break;
            }
        }
        if (isset($params['filters'])) {
                $sp_filter = $params['filters'];
        }
        $movies = array();
        $listable = array(
            'contentRating' => array(),
            'genre' => array()
        );
        $crlist = array();
        $glist = array();
        $lid = $args['id'];
        try {
            $res = $this->getClient()->get('library/sections/'.$lid.'/all');
            $directories = new \SimpleXMLElement($res->getBody()->getContents());

            foreach($directories as $directory) {
                $rating = $this->ageRating((string)$directory['contentRating']);
                $genres = array();
                foreach($directory->Genre as $g) {
                    $genres[] = (string)$g['tag'];
                }
                $movies[] =  array(
                    'id' => (int)$directory->Media['id'],
                    'title' => (string)$directory['title'],
                    'year' => (string)$directory['year'],
                    'contentRating' => $rating,
                    'summary' => (string)$directory['summary'],
                    'thumb' => $this->buildUrl((string)$directory['thumb'], true),
                    'art' => $this->buildUrl((string)$directory['art'], true),
                    'genres' => implode(', ', $genres)
                );
                if (array_search($rating, $crlist) === false) {
                    $crlist[] = $rating;
                }
                foreach($genres as $g) {
                    if (array_search($g, $glist) === false) {
                        $glist[] = $g;
                    }
                }
            }
            foreach($crlist as $cr) {
                $listable['contentRating'][] = array("value" => $cr, "text" => $cr);
            }
            foreach($glist as $g) {
                $listable['genres'][] = array("value" => $g, "text" => $g);
            }
        }catch(GuzzleHttp\Exception\ClientException $exception) {
            return $this->evaluateRequest($res, $response, $exception);
        }
        return $response->write(json_encode($this->sprunjify($movies, $sp_size, $sp_page, $sp_sorttype, $sp_direction, $listable, $sp_filter), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT))->withStatus(200);
    }
}