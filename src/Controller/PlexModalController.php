<?php

namespace UserFrosting\Sprinkle\Plex\Controller;

use Interop\Container\ContainerInterface;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use GuzzleHttp;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\UserProfile\Database\Models\ProfileFields;

class PlexModalController extends SimpleController {

    public function getHowTo($request, $response, $args)
    {
        return $this->ci->view->render($response, 'modals/howto.html.twig');
    }
}