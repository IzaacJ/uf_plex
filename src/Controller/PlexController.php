<?php

namespace UserFrosting\Sprinkle\Plex\Controller;

use Interop\Container\ContainerInterface;
use Slim\Http\Response;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use GuzzleHttp;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\UserProfile\Database\Models\ProfileFields;

class PlexController extends SimpleController {
    protected $ci;

    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);
        $this->ci = $ci;
    }

    private function getUserToken() {
        $user = User::where('id', $this->ci->currentUser['id'])->first();
        $plextoken = ProfileFields::where('parent_id', $user['id'])->where('slug', 'plex-token')->value('value');
        return $plextoken;
    }

    protected function buildUrl($path = null, $token = false) {
        $server = $this->ci->config['plex.server'];
        $url = 'http' . ($server['ssl']?'s':'') . '://' . $server['host'] . ':' . $server['port'];
        if ($path !== null) {
            $url .= $path;
            if($token === true) {
                $url .= '?X-Plex-Token=' . $this->getUserToken();
            }
        }
        return $url;
    }

    protected function getClient($token = null) {
        return new GuzzleHttp\Client([
            'base_uri' => $this->buildUrl(),
            'headers' => [
                'X-Plex-Token' => ($token === null ? $this->getUserToken() : $token)
            ]
        ]);
    }

    public function showLibraries($request, $response, $args) {
        $libraries = (new PlexApiController($this->ci))->getLibraries($request, new Response(), $args)->getBody();

        return $this->ci->view->render($response, 'pages/libraries.html.twig', [
            'libraries' => json_decode($libraries)
        ]);
    }

    public function showLibrary($request, $response, $args) {
        $library = (new PlexApiController($this->ci))->getLibraryInfo($request, new Response(), $args)->getBody();

        return $this->ci->view->render($response, 'pages/library.html.twig', [
            'library' => json_decode($library),
            'library_id' => $args['id']
        ]);
    }
}