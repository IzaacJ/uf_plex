<?php
/**
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/licenses/UserFrosting.md (MIT License)
 *
 * US English message token translations for the 'account' sprinkle.
 *
 * @package userfrosting\i18n\en_US
 * @author Alexander Weissman
 */
 
return [
    "PLEXTOKEN" => [
        "@TRANSLATION" => "Plex Token",

        "ENTER"        => "Enter your Plex Token",

        "HOW"           => "How-To: Get Your Plex Token",
        "HOWTO"         => "To register an account with {{site_title}}, you need the <a {{link_attributes | raw}}>Plex Token</a>.",
        "HOW_FOR"       => "How-To: Get Your Plex Token for {{title}}",
    ],

    "LIBRARY" => [
        1 => "Library",
        2 => "Libraries",

        "TYPE" => "Type",

        "PAGE_DESCRIPTION" => [
            "LIBRARY" => "List all movies in the library.",
            "LIBRARIES" => "All libraries on the {{title}} server."
        ],

        "MOVIE" => [
            1 => "Movie",
            2 => "Movies",

            "THUMB" => "Cover",
            "TITLE" => "Title",
            "YEAR" => "Year",
            "GENRE" => "Genre",
            "CONTENTRATING" => "CR",
            "SUMMARY" => "Summary"
        ]
    ]
];
