<?php
/**
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/licenses/UserFrosting.md (MIT License)
 *
 * US English message token translations for the 'account' sprinkle.
 *
 * @package userfrosting\i18n\en_US
 * @author Alexander Weissman
 */

return [
	"VALIDATE" => [
        "PLEXTOKEN" => "Something's off?"
    ]
];
