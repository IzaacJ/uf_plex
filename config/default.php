<?php
return [
    'plex' => [
        'server' => [
            'host' => 'localhost',
            'port' => '32400',
            'ssl' => false
        ]
    ]
];