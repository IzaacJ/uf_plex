# Plex Sprinkle for [UserFrosting 4](https://www.userfrosting.com)

Plex sprinkle for [UserFrosting 4](https://www.userfrosting.com). Lets you view Plex information.

> This version only works with UserFrosting 4.1.x !

# Help and Contributing

If you need help using this sprinkle or found any bug, feels free to open an issue or submit a pull request. You can also find me on the [UserFrosting Chat](https://chat.userfrosting.com/) most of the time for direct support. 

<a href='https://ko-fi.com/izaacj' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

# Installation

Edit UserFrosting `app/sprinkles.json` file and add the following to the `require` list : `"izaacj/uf_plex": "^2.0.0"`. Also add `FormGenerator`, `UserProfile` and `Plex` to the `base` list. For example:

```
{
    "require": {
        "izaacj/uf_plex": "^2.0.0"
    },
    "base": [
        "core",
        "account",
        "admin",
        "FormGenerator",
        "UserProfile",
        "Plex"
    ]
}
```

Run `composer update` then `php bakery bake` to install the sprinkle.

## Permissions
Only the root user can use these permissions after a fresh install. To give access to another user, simply add the `see_plex_token` and/or `edit_plex_token` permission slug to that user role. 

## Add link to the menu
The default pages are available at:
```
{site.url}/libraries
{site.url}/library/{id}
{site.url}/movie/{id}
```
Feel free to add them to the menu, like this:
```
{% if checkAccess('uri_user') %}
<li>
    <a href="{{site.uri.public}}/libraries"><i class="fa fa-film fa-fw"></i> <span>{{ translate("LIBRARY", 2) }}</span></a>
</li>
{% endif %}
```

## Adding custom config

! TODO

> *NOTE* Only `.json` are accepted. `Yaml` schemas are cannot be used for now.

# Licence

By [Izaac Johansson](https://github.com/izaacj). Copyright (c) 2018, free to use in personal and commercial software as per the MIT license.